FROM openjdk:8-alpine
ADD build/libs/*.jar main.jar
ENTRYPOINT ["java", "-jar", "main.jar"]
